# Backend
A simple Flask app. Since `docker-compose.yml` is in the project root, whereas
a normal flask app would be just the flask app, here the flask app starts
under the `app` directory.

For expandability, the "api" is handled as a blueprint.
