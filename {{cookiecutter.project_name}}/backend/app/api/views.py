import os, redis
from flask import Blueprint, request, jsonify, current_app, g, url_for
from rq import push_connection, pop_connection, Queue
from {{cookiecutter.py_module}} import __name__

from .utils import do_something, predict_model, predict_served_model

bp = Blueprint('api', __name__)

version = os.getenv('API_VERSION', default='0.0.0')

API_BASE = '/api/{}/'.format(version)


def get_redis_connection():
    redis_connection = getattr(g, '_redis_connection', None)
    if redis_connection is None:
        redis_url = current_app.config['REDIS_URL']
        redis_connection = g._redis_connection = redis.from_url(redis_url)
    return redis_connection

@bp.before_request
def push_rq_connection():
    push_connection(get_redis_connection())

@bp.teardown_request
def pop_rq_connection(exception=None):
    pop_connection()

@bp.route(API_BASE+'task/<task_id>', methods=['GET'])
def check_task(task_id):
    q = Queue()
    task = q.fetch_job(task_id)
    if task is None:
        response = {
            'status': 'unknown',
            'message': 'no task with id: {}'.format(task_id)
        }

    else:
        response = {
            'task_id': task.get_id(),
            'status': task.get_status(),
            'result': task.result,
        }
        if task.is_failed:
            msg = task.exc_info.strip().split('\n')[-1]
            response['message'] = msg
    return jsonify(response)

@bp.route(API_BASE+'model-task', methods=['GET'])
def model_task():
    uri = '{}model-task'.format(API_BASE)
    q = Queue()
    task = q.enqueue(predict_model)

    res_uri = '{}task/{}'.format(API_BASE, task.get_id())
    response = {
        'status': 'success',
        'data': {
            'task_id': task.get_id(),
            'result_uri': res_uri,
            'uri': uri,
        }
    }

    return jsonify(response)

@bp.route(API_BASE+'model-serve-task', methods=['GET'])
def model_serve_task():
    uri = '{}model-serve-task'.format(API_BASE)
    q = Queue()
    task = q.enqueue(predict_served_model)

    res_uri = '{}task/{}'.format(API_BASE, task.get_id())
    response = {
        'status': 'success',
        'data': {
            'task_id': task.get_id(),
            'result_uri': res_uri,
            'uri': uri,
        }
    }

    return jsonify(response)

@bp.route(API_BASE+'/demo-task', methods=['GET'])
def run_task():
    uri = 'demo-task'
    q = Queue()
    task = q.enqueue(do_something, '4u')

    res_uri = 'task/{}'.format(task.get_id())
    response = {
        'status': 'success',
        'data': {
            'task_id': task.get_id(),
            'result_uri': res_uri,
            'uri': uri,
        }
    }

    return jsonify(response)


@bp.route('/')
def index():
    return '{} server'.format(__name__)

@bp.route(API_BASE)
def api_base():
    return '{} server\'s api {}'.format(__name__, api_version)
