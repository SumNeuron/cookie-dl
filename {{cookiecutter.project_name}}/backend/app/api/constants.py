import os
CONSTANTS_DIR = os.path.dirname(os.path.realpath(__file__))
SERVED_MODEL_DIR = os.path.join(CONSTANTS_DIR, '..', 'models', 'serving')
