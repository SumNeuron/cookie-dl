'''-----------------------------------------------------------------------------
IMPORTS
-----------------------------------------------------------------------------'''
import os

# for main flask app
from flask import Flask
from flask_cors import CORS

# blueprints of the app
from .api.views import (bp as bp_api)
from . import settings

from {{cookiecutter.py_module}} import __name__

def create_app():

    app = Flask(__name__)
    app.register_blueprint(bp_api)
    app.config.from_object(settings)
    CORS(app)
    return app
