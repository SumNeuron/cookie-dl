__name__ = '{{cookiecutter.py_module}}'
major = 0
minor = 0
patch = 0
__version__ = '{}.{}.{}'.format(major, minor, patch)


__author__ = "{{cookiecutter.author}}"
__author_email__ = '{}@email.com'.format('.'.join(__author__.split(' ')))
