import argparse, subprocess, os

parser = argparse.ArgumentParser(description=(
    'For those too lazy to type (nvidia-)docker in the shell.\n'
    '\tai: a tensorflow container for running and training the model.\n'
), formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument(
    '-w', '--which',
    help='which docker-compose to spin up',
    choices=['ai'],
    type=str,
    default='web'
)

parser.add_argument(
    '-p', '--port',
    help='port to map',
    default="8888",
)

parser.add_argument(
    '-c', '--command',
    help='what to do with docker-compose',
    choices=['build', 'up', 'down'],
    type=str,
    default='up'
)

parser.add_argument(
    '-nc', '--no-cache',
    help='no cache',
    action="store_true",
    default=False
)

parser.add_argument(
    '-n', '--nvidia-docker',
    help='nvidia',
    action="store_true",
    default=False
)


args = parser.parse_args()

os.environ['DOCKER_BUILDKIT'] = '1'
os.environ['COMPOSE_DOCKER_CLI_BUILD'] = '1'

command = []
if args.nvidia_docker:
    command.append('nvidia-docker')
else:
    command.append('docker')

if args.command == 'up':
    command += [
        'run', '-it', '-d'
        '--mount type=bind,source="$(pwd)"/notebooks,target=/tf/notebooks',
        '--mount type=bind,source="$(pwd)"/data,target=/tf/data' ,
        '--mount type=bind,source="$(pwd)"/{{cookiecutter.py_module}},target=/tf/{{cookiecutter.py_module}}' ,
        '--mount type=bind,source="$(pwd)"/experiments,target=/tf/experiments' ,
        '--mount type=bind,source="$(pwd)"/models,target=/tf/models',
    ]
    if args.nvidia_docker:
        command += ['--gpus all',]

    command += [
        '--name {{cookiecutter.py_module}}_{}'.format(args.which),
        '-p {}:8888'.format(args.port),
        '{{cookiecutter.py_module}}:{}'.format(args.which)
    ]

if args.command == 'build':
    command += [
        'build',
         '-f', 'Dockerfile.{}'.format(args.which),
         '-t', '{{cookiecutter.py_module}}:{}'.format(args.which),
         '.'
    ]
    if args.no_cache:
        command.append('--no-cache')

if args.command == 'down':
    command += [
        'stop',
        '{{cookiecutter.py_module}}:{}'.format(args.which),
    ]



subprocess.run(command)
