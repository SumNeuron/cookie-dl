import axios from 'axios'

const apiVersion = `v${process.env.API_VERSION}`
const apiBaseUrl = process.env.PUBLIC_API_URL
const api = axios.create({
  baseURL: `${apiBaseUrl}/api/${apiVersion}/`
})

export default api
