import api from '@/plugins/axios'

export const modelInquiry = async ({
  input='isIrrelevantForToyExample'
})=>{
  let response = await api.get(`model-task`)
  let payload = response.data
  if (payload) {
    let status = payload.status
    let data = payload.data
    return payload
  }
}

export const servedModelInquiry = async ({
  input='isIrrelevantForToyExample'
})=>{
  let response = await api.get(`model-serve-task`)
  let payload = response.data
  if (payload) {
    let status = payload.status
    let data = payload.data
    return payload
  }
}
