const state = () => ({
  user: null,
  tasks: {},
  taskResults: {}
})

export default state
