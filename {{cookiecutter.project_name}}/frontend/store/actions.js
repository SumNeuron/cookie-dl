/* NOTE: this uses both default axios and a custom instance
 * of axios defined in /frontend/plugins/axios.js
 */
import axios from 'axios'
import api from '@/plugins/axios'

// NOTE: various utils for handling hand off to / from api
import {modelInquiry, servedModelInquiry} from '@/helpers/api.js'
import {checkTaskUntilDone} from '@/helpers/tasks.js'
import {isObjectEmpty} from '@/helpers/utils.js'

const actions = {
  /** NOTE: related to restricted pages set up under:
  * - /frontend/restriced_pages/index.js,
  * - /frontent/middleware/auth.js
  * - /frontent/nuxt.config.js
  */
  nuxtServerInit ({ commit }, { req }) {
    if (req.session && req.session.authUser) {
      commit('set', {k:'user', v:req.session.authUser})
    }
  },

  async login ({ commit }, { username, password }) {
    try {
      const { data } = await axios.post('/restricted_pages/login', { username, password })
      commit('set', {k:'user', v:data})
    } catch (error) {
      if (error.response && error.response.status === 401) {
        throw new Error('Bad credentials')
      }
      throw error
    }
  },

  async logout ({ commit }) {
    await axios.post('/restricted_pages/logout')
    commit('set', {k:'user', v:null})
  },

  // enqueue task
  async enqueueModelInquiry({state, commit, dispatch}, {input='toyExample'}) {
    let taskName = input.toLowerCase()
    // task already submitted
    if (state.tasks[taskName] !== undefined) return
    // task already finished
    if (state.taskResults[taskName] !== undefined) return

    let task = await modelInquiry({input})
    commit('sset', {k: 'tasks', v: task, s: taskName})
    return await dispatch('checkModelTask', {input})
  },

  async enqueueServedModelInquiry({state, commit, dispatch}, {input='toyExample'}) {
    let taskName = input.toLowerCase()
    // task already submitted
    if (state.tasks[taskName] !== undefined) return
    // task already finished
    if (state.taskResults[taskName] !== undefined) return

    let task = await servedModelInquiry({input})
    commit('sset', {k: 'tasks', v: task, s: taskName})
    return await dispatch('checkModelTask', {input})
  },

  // Check task until it fails and recall if need be
  async checkModelTask({state, dispatch, commit}, {input='toyExample'}) {
    let taskName = input.toLowerCase()
    let task = state.tasks[taskName]
    // no task yet
    if (task === undefined) return

    // task already done, no need to poll for it
    if (state.taskResults[taskName] !== undefined) return

    // what is task id?
    let taskId = task.data.task_id

    // start polling
    let taskResult =  await checkTaskUntilDone(taskId)

    // oh no, no data, clear task to be resubmitted
    if (isObjectEmpty(taskResult)) {
      commit('sset', {k: 'tasks', v: undefined, s: taskName})
      // resubmit request
      dispatch('enqueueModelInquiry', {input})
      return
    }

    // got results, set them
    await commit('sset', {k: 'taskResults', v: taskResult, s: taskName})
    return taskResult
  }
}


export default actions
