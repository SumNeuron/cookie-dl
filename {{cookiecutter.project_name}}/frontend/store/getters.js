const getters = {
  getTask: (state) => (name) => state.tasks[name],
  getTaskResult: (state) => (name) => state.taskResults[name]
}

export default getters
