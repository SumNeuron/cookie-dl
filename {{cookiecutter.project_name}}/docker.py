import os, argparse, subprocess

parser = argparse.ArgumentParser(description='For those too lazy to type docker-compose in the shell.')
parser.add_argument(
    '-w', '--which',
    help='which docker-compose to spin up',
    choices=['ai', 'be', 'fe'],
    type=str,
    default='web'
)

parser.add_argument(
    '-p', '--production',
    help='mode to spin docker up in',
    action="store_true",
    default=False
)

parser.add_argument(
    '-c', '--command',
    help='what to do with docker-compose',
    choices=['build', 'up', 'down'],
    type=str,
    default='up'
)

parser.add_argument(
    '-nc', '--no-cache',
    help='no cache',
    action="store_true",
    default=False
)

args = parser.parse_args()

os.environ['DOCKER_BUILDKIT'] = '1'
os.environ['COMPOSE_DOCKER_CLI_BUILD'] = '1'

command = ['docker-compose']
if args.which in ['be', 'fe']:
    command.append('-f')
    command.append('docker-compose.{}.production.yml'.format('be'))
    if args.production == False:
            command.append('-f')
            command.append('docker-compose.{}.development.yml'.format('be'))

if args.which == 'fe':
    command.append('-f')
    command.append('docker-compose.{}.production.yml'.format('fe'))
    if args.production == False:
            command.append('-f')
            command.append('docker-compose.{}.development.yml'.format('fe'))

if args.which == 'ai':
    command.append('-f')
    command.append('docker-compose.{}.development.yml'.format(args.which))

command.append(args.command)
if args.no_cache:
    command.append('--no-cache')
subprocess.run(command)
