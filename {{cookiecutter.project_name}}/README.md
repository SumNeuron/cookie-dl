# {{cookiecutter.project_name}}

Contents include:


- backend: Flask, Redis, and rq backend API for the deployment of the trained model.
- data: Directory for scripts to download and process data.
- experiments: Directory for storing results of exploring hyperparameter space.
- frontend: Nuxt, Vue, and Vuetify frontend for user-friendly deployment of the trained model.
- models: Saved models.
- notebooks: Juypter notebooks and playgrounds for running / exploring the model.
- scripts: Script versions of the notebooks.
- {{cookiecutter.py_module}}: python module with functions required for creating, training, and evaluating the model.
