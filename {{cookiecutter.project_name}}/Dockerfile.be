FROM debian:buster-slim

# RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    python3 \
    python3-pip \
    python3-setuptools\
    python3-dev


# Layer requriments and install before copying files as
# requirments are less likely to change
COPY ./backend/requirements.apt /app/
# --- Install any needed packages specified in requirements.apt
RUN apt-get update && xargs apt-get install -y < /app/requirements.apt

COPY ./backend/requirements.pip /app/
# --- Install any needed packages specified in requirements.pip
RUN pip3 install -r /app/requirements.pip

# specific dependencies may need new version of wheel
RUN pip3 uninstall wheel -y
RUN pip3 install wheel

# add custom python module
COPY ./{{cookiecutter.py_module}} /{{cookiecutter.py_module}}
RUN pip3 install -e /{{cookiecutter.py_module}}

# Change into app
WORKDIR /app

# Add contents of "backend" sub-project dir to /app/
COPY ./backend /app/

# add model to /app/model as used in backend/app/api/utils.py
COPY ./models /app/models
